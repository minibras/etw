const Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    class Adresse extends Sequelize.Model {}
    Adresse.init({
        id: {
            type: DataTypes.INTEGER(10),
            autoIncrement: true,
            allowNull: false,
            primaryKey: true
        },
        ligne1: {
            type: DataTypes.STRING(250),
            allowNull: false
        },
        ligne2: {
            type: DataTypes.STRING(250)
        },
        complement: {
            type: DataTypes.STRING(250)
        },
        id_ville: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            references: {
                model: 'ville',
                key: 'id'
            }
        }
    }, {
        sequelize,
        freezeTableName: true,
        timestamps: false,
        modelName: 'adresse'
    })
    return Adresse;
}
