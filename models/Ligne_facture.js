const Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    class Ligne_facture extends Sequelize.Model {}
    Ligne_facture.init({
        id: {
            type: DataTypes.INTEGER(10),
            autoIncrement: true,
            allowNull: false,
            primaryKey: true
        },
        description: {
            type: DataTypes.STRING(250),
            allowNull: false
        },
        quantite: {
            type: DataTypes.INTEGER(4),
            allowNull: false
        },
        id_produit: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            references: {
                model: 'produit',
                key: 'id'
            }
        },
        id_tva: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            references: {
                model: 'tva',
                key: 'id'
            }
        },
        id_facture: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            references: {
                model: 'facture',
                key: 'id'
            }
        }
    }, {
        sequelize,
        freezeTableName: true,
        timestamps: false,
        modelName: 'ligne_facture'
    })
    return Ligne_facture;
}
