const Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    class Pays extends Sequelize.Model {}
    Pays.init({
        id: {
            type: DataTypes.INTEGER(10),
            autoIncrement: true,
            allowNull: false,
            primaryKey: true
        },
        pays: {
            type: DataTypes.STRING(250),
            allowNull: false
        }
    }, {
        sequelize,
        freezeTableName: true,
        timestamps: false,
        modelName: 'pays'
    })
    return Pays;
}
