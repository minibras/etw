const Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    class Type_adresse extends Sequelize.Model {}
    Type_adresse.init({
        id: {
            type: DataTypes.INTEGER(10),
            autoIncrement: true,
            allowNull: false,
            primaryKey: true
        },
        libelle: {
            type: DataTypes.STRING(50),
            allowNull: false
        }
    }, {
        sequelize,
        freezeTableName: true,
        timestamps: false,
        modelName: 'type_adresse'
    })
    return Type_adresse;
}
