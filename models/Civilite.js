const Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    class Civilite extends Sequelize.Model {}
    Civilite.init({
        id: {
            type: DataTypes.INTEGER(10),
            autoIncrement: true,
            allowNull: false,
            primaryKey: true
        },
        libelle: {
            type: DataTypes.STRING(30)
        }
    }, {
        sequelize,
        freezeTableName: true,
        timestamps: false,
        modelName: 'civilite'
    })
    return Civilite;
}
