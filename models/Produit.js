const Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    class Produit extends Sequelize.Model {}
    Produit.init({
        id: {
            type: DataTypes.INTEGER(10),
            autoIncrement: true,
            allowNull: false,
            primaryKey: true
        },
        reference: {
            type: DataTypes.STRING(10),
            allowNull: false
        },
        designation: {
            type: DataTypes.STRING(250),
            allowNull: false
        },
        prix_unitaire_ht: {
            type: DataTypes.DECIMAL(10, 2),
            allowNull: false
        },
        quantite_stock: {
            type: DataTypes.INTEGER(5),
            allowNull: false
        },
        est_dispo: {
            type: DataTypes.BOOLEAN
        },
        date_dispo: {
            type: DataTypes.DATE
        },
        id_categorie: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            references: {
                model: 'categorie',
                key: 'id'
            }
        },
        id_marque: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            references: {
                model: 'marque',
                key: 'id'
            }
        },
        id_reduction: {
            type: DataTypes.INTEGER(11),
            references: {
                model: 'reduction',
                key: 'id'
            }
        },
        id_pays: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            references: {
                model: 'pays',
                key: 'id'
            }
        }
    }, {
        sequelize,
        freezeTableName: true,
        timestamps: false,
        modelName: 'produit'
    })
    return Produit;
}
