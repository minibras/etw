const Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    class Tva extends Sequelize.Model {}
    Tva.init({
        id: {
            type: DataTypes.INTEGER(10),
            autoIncrement: true,
            allowNull: false,
            primaryKey: true
        },
        taux: {
            type: DataTypes.DECIMAL(4,2),
            allowNull: false
        },
        actif: {
            type: DataTypes.BOOLEAN,
            allowNull: false
        }
    }, {
        sequelize,
        freezeTableName: true,
        timestamps: false,
        modelName: 'tva'
    })
    return Tva;
}
