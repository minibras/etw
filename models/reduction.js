const Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    class Reduction extends Sequelize.Model {}
    Reduction.init({
        id: {
            type: DataTypes.INTEGER(10),
            autoIncrement: true,
            allowNull: false,
            primaryKey: true
        },
        valeur_pourcentage: {
            type: DataTypes.STRING(150)
        },
        valeur_reduction: {
            type: DataTypes.STRING(150)
        },
        code_promo: {
            type: DataTypes.STRING(150)
        }
    }, {
        sequelize,
        freezeTableName: true,
        timestamps: false,
        modelName: 'reduction'
    })
    return Reduction;
}
