const Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    class Commande extends Sequelize.Model {}
    Commande.init({
        id: {
            type: DataTypes.INTEGER(10),
            autoIncrement: true,
            allowNull: false,
            primaryKey: true
        },
        date_commande: {
            type: DataTypes.DATE,
            allowNull: false
        },
        demande_retour: {
            type: DataTypes.BOOLEAN,
            allowNull: false
        },
        id_client: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            references: {
                model: 'client',
                key: 'id'
            }
        },
        id_moyen_paiement: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            references: {
                model: 'moyen_paiement_utilise',
                key: 'id'
            }
        },
        id_facture: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            references: {
                model: 'facture',
                key: 'id'
            }
        }
    }, {
        sequelize,
        freezeTableName: true,
        timestamps: false,
        modelName: 'commande'
    })
    return Commande;
}
