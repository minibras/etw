const Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    class Categorie extends Sequelize.Model {}
    Categorie.init({
        id: {
            type: DataTypes.INTEGER(10),
            autoIncrement: true,
            allowNull: false,
            primaryKey: true
        },
        libelle: {
            type: DataTypes.STRING(250)
        }
    }, {
        sequelize,
        freezeTableName: true,
        timestamps: false,
        modelName: 'categorie'
    })
    return Categorie;
}
