const Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    class Moyen_paiement_utilise extends Sequelize.Model {}
    Moyen_paiement_utilise.init({
        id: {
            type: DataTypes.INTEGER(10),
            autoIncrement: true,
            allowNull: false,
            primaryKey: true
        },
        valeur: {
            type: DataTypes.STRING(250),
            allowNull: false
        },
        libelle: {
            type: DataTypes.STRING(250),
            allowNull: false
        }
    }, {
        sequelize,
        freezeTableName: true,
        timestamps: false,
        modelName: 'moyen_paiement_utilise'
    })
    return Moyen_paiement_utilise;
}
