const Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    class Marque extends Sequelize.Model {}
    Marque.init({
        id: {
            type: DataTypes.INTEGER(10),
            autoIncrement: true,
            allowNull: false,
            primaryKey: true
        },
        libelle: {
            type: DataTypes.STRING(250)
        }
    }, {
        sequelize,
        freezeTableName: true,
        timestamps: false,
        modelName: 'marque'
    })
    return Marque;
}
