const Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    class Client extends Sequelize.Model {}
    Client.init({
        id: {
            type: DataTypes.INTEGER(10),
            autoIncrement: true,
            allowNull: false,
            primaryKey: true
        },
        nom: {
            type: DataTypes.STRING(50),
            allowNull: false
        },
        prenom: {
            type: DataTypes.STRING(50),
            allowNull: false
        },
        date_naissance: {
            type: DataTypes.DATE(50),
            allowNull: false
        },
        mot_de_passe: {
            type: DataTypes.STRING(300),
            allowNull: false
        },
        id_civilite: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            references: {
                model: 'civilite',
                key: 'id'
            }
        }
    }, {
        sequelize,
        freezeTableName: true,
        timestamps: false,
        modelName: 'client'
    })
    return Client;
}