const Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    class Livrer extends Sequelize.Model {}
    Livrer.init({
        date_livraison: {
            type: DataTypes.DATE,
            allowNull: false
        },
        preuve: {
            type: DataTypes.STRING(10),
            allowNull: false
        },
        est_livre: {
            type: DataTypes.BOOLEAN,
            allowNull: false
        },
        id_adresse: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            references: {
                model: 'adresse',
                key: 'id'
            }
        },
        id_facture: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            references: {
                model: 'facture',
                key: 'id'
            }
        }
    }, {
        sequelize,
        freezeTableName: true,
        timestamps: false,
        modelName: 'livrer'
    })
    return Livrer;
}
