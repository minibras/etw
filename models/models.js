
const Sequelize = require('sequelize');

require('dotenv').config();

//connexion à la base de donnée en utilisant sequelize
const sequelize = new Sequelize(process.env.dbName, process.env.dbUser, process.env.dbPass, {
    host: process.env.dbHost,
    port: process.env.dbPort,
    dialect: process.env.dbType 
});


//vérification de la connexion
sequelize
    .authenticate()
    .then(() => {
        console.log('Connection has been established successfully.');
    })
    .catch(err => {
        console.error('Unable to connect to the database:', err);
    });



//tableau comportant les noms des tables :
const models = [
    'Civilite',
    'Code_postal',
    'Type_adresse',
    'Categorie',
    'Marque',
    'Reduction',
    'Tva',
    'Moyen_paiement_utilise',
    'Ville',
    'Adresse',
    'Client',
    'Moyen_paiement_enregistre',
    'Moyen_communication',
    'Pays',
    'Produit',
    'Devis',
    'Ligne_devis',
    'Facture',
    'Ligne_facture',
    'Commande',
    'Livrer',
    'Commentaire',
    'Posseder'
    
];

//on import chaque model avec une boucle forEach :
models.forEach(model=> {
    module.exports[model] = sequelize.import(__dirname + '/' + model);
});

module.exports.sequelize = sequelize;