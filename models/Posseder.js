const Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    class Produit extends Sequelize.Model {}
    Produit.init({
        id_client: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            references: {
                model: 'client',
                key: 'id'
            }
        },
        id_adresse: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            references: {
                model: 'adresse',
                key: 'id'
            }
        },
        id_type_adresse: {
            type: DataTypes.INTEGER(11),
            references: {
                model: 'type_adresse',
                key: 'id'
            }
        }
    }, {
        sequelize,
        freezeTableName: true,
        timestamps: false,
        modelName: 'produit'
    })
    return Produit;
}
