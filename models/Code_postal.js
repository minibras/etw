const Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    class Code_postal extends Sequelize.Model {}
    Code_postal.init({
        id: {
            type: DataTypes.INTEGER(10),
            autoIncrement: true,
            allowNull: false,
            primaryKey: true
        },
        valeur: {
            type: DataTypes.STRING(6)
        }
    }, {
        sequelize,
        freezeTableName: true,
        timestamps: false,
        modelName: 'code_postal'
    })
    return Code_postal;
}
