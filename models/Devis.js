const Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    class Devis extends Sequelize.Model {}
    Devis.init({
        id: {
            type: DataTypes.INTEGER(10),
            autoIncrement: true,
            allowNull: false,
            primaryKey: true
        },
        date_devis: {
            type: DataTypes.DATE,
            allowNull: false
        },
        duree_validite: {
            type: DataTypes.DATE,
            allowNull: false
        },
        est_liste_souhait: {
            type: DataTypes.BOOLEAN
        },
        id_client: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            references: {
                model: 'client',
                key: 'id'
            }
        }
    }, {
        sequelize,
        freezeTableName: true,
        timestamps: false,
        modelName: 'devis'
    })
    return Devis;
} 