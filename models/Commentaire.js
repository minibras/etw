const Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    class Commentaire extends Sequelize.Model {}
    Commentaire.init({
        id: {
            type: DataTypes.INTEGER(10),
            autoIncrement: true,
            allowNull: false,
            primaryKey: true
        },
        titre: {
            type: DataTypes.STRING(30),
        },
        description: {
            type: DataTypes.STRING(250),
        },
        id_client: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            references: {
                model: 'client',
                key: 'id'
            }
        },
        id_produit: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            references: {
                model: 'produit',
                key: 'id'
            }
        }
    }, {
        sequelize,
        freezeTableName: true,
        timestamps: false,
        modelName: 'livrer'
    })
    return Commentaire;
}
