const Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    class Ville extends Sequelize.Model {}
    Ville.init({
        id: {
            type: DataTypes.INTEGER(10),
            autoIncrement: true,
            allowNull: false,
            primaryKey: true
        },
        libelle: {
            type: DataTypes.STRING(250),
            allowNull: false
        },
        id_code_postal: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            references: {
                model: 'code_postal',
                key: 'id'
            }
        }
    }, {
        sequelize,
        freezeTableName: true,
        timestamps: false,
        modelName: 'ville'
    })
    return Ville;
}
