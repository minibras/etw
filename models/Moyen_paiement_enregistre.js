const Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    class Moyen_paiement_enregistre extends Sequelize.Model {}
    Moyen_paiement_enregistre.init({
        id: {
            type: DataTypes.INTEGER(10),
            autoIncrement: true,
            allowNull: false,
            primaryKey: true
        },
        valeur: {
            type: DataTypes.STRING(250),
            allowNull: false
        },
        libelle: {
            type: DataTypes.STRING(250),
            allowNull: false
        },
        id_client: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            references: {
                model: 'client',
                key: 'id'
            }
        }
    }, {
        sequelize,
        freezeTableName: true,
        timestamps: false,
        modelName: 'moyen_paiement_enregistre'
    })
    return Moyen_paiement_enregistre;
}
