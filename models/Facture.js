const Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    class Facture extends Sequelize.Model {}
    Facture.init({
        id: {
            type: DataTypes.INTEGER(10),
            autoIncrement: true,
            allowNull: false,
            primaryKey: true
        },
        date_facture: {
            type: DataTypes.DATE,
            allowNull: false
        },
        est_paye: {
            type: DataTypes.BOOLEAN,
            allowNull: false
        },
        id_client: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            references: {
                model: 'client',
                key: 'id'
            }
        },
        id_devis: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            references: {
                model: 'devis',
                key: 'id'
            }
        },
        id_adresse: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            references: {
                model: 'adresse',
                key: 'id'
            }
        }
    }, {
        sequelize,
        freezeTableName: true,
        timestamps: false,
        modelName: 'facture'
    })
    return Facture;
}
