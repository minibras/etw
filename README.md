# EatTheWorld - Vente produits alimentaires mondiaux

installer node js : https://nodejs.org/en/
installer nvm : https://github.com/creationix/nvm/blob/master/README.md (linux)
ou nvm windows : https://github.com/coreybutler/nvm-windows (ça ne fonctionne pas sur l'ordi cgi apparemment)

nvm permet de gerer la version de node que l'on utilise sur la machine.

Récupérer le repository (git clone) :
dans votre workspace :
 * `git clone https://gitlab.com/minibras/etw.git`

Ensuite :
 * se placer à la racine du projet (cd : change directory) : `cd etw`
 * executer la commande : `npm install` (installation des dépendances)
 * lancer la base de donnée(pour moi c'est sur mamp)
 * démarrer le serveur : `npm start` (port par défaut : 3000)
 
Pour faire une mise à jour de depot local (votre machine): `git pull`

Pour mettre a jour le depot distant (git lab) :
**ATTENTION** toujours faire un `git pull` avant de mettre a jour le dépot distant **!!!!**
 * `git add -A` pour ajouter tout les fichiers à la maj
 * `git commit -m "vosModifications"` pour commiter les modifications et mettre un petit message
 * `git push ` pour mettre à jour le dépot distant
 
concernant la db : 
créer un fichier **.env** à la racine du projet.
dans ce fichier créer les propriété suivante :
 * dbHost=localhost
 * dbUser= *votreUserMysql*
 * dbPass= *votrePasswordMysql*
 * dbName=is_etw
 * dbType=mysql
 
 
Test des requetes via sequelize :
dans le dossier routes il y a une route nommée requete.js et il y a une vue requete.hbs dans le dossier view.
la route et la vue sont associé dans le fichier routes/dynamicRoute.json de manière à ce que le resultat de la requete soit affiché lors de la connexion a l'adresse http://localhost:3000/req.

construction de la requete sequelize : 
database.Ville.findAll({}).then(ville => {
    res.render(req.message.view, {
            title: 'test requete',
            result: JSON.stringify(ville)
}).catch(error => {
    console.log("erreur lors du findAll : " + error);
});

cette requete va recupérer toutes les villes et les renvoyer au front: 
res.render() permet de renvoyer des données vers le front
result est le resultat de la requete qui est rendu lisible (grace a JSON.stringify) et qui est affiché sur le front grace à la balise {{result}}.

Concernant le front : 
utilisation de handlebar.js (hbs), il s'agit d'un moteur de template.
hbs permet de mettre en place un layout(gabarit de page)
il intègre aussi des helpers (fonction utilisable pour traiter les données renvoyés au front)
j'ai ajouté un exemple sur le lien : http://localhost:3000/exempleHbs



**Infos pratiques :**

Trello : https://trello.com/b/kAzbfvzo/projet

.gitignore : référence la liste des éléments à ignorer lors du push. (attention si vous le modifiez)

loi concernant le stockage des infos bancaire : https://www.cnil.fr/fr/le-paiement-distance-par-carte-bancaire

ORM sequelize : http://docs.sequelizejs.com/manual/getting-started