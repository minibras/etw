const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const fs = require('fs');
const hbs = require('hbs');

const dataBase = require('./models/models');

/*ajout partials via hbs*/
hbs.registerPartials(__dirname + '/views/partials',function() {
    console.log('partials registered');
});

const app = express();

//association des foreigns keys :
//ville -> code_postal
dataBase.Ville.belongsTo(dataBase.Code_postal, {
    foreignKey: "id_code_postal",
    target: "id"
});
dataBase.Code_postal.hasMany(dataBase.Ville, {
    foreignKey: "id_code_postal"
});
//adresse -> ville
dataBase.Adresse.belongsTo(dataBase.Ville, {
    foreignKey: "id_ville",
    target: "id"
});
dataBase.Ville.hasMany(dataBase.Adresse, {
    foreignKey: "id_ville"
});
//moyen_paiement_enregistre -> client
dataBase.Moyen_paiement_enregistre.belongsTo(dataBase.Client, {
    foreignKey: "id_client",
    target:"id"
});
dataBase.Client.hasMany(dataBase.Moyen_paiement_enregistre, {
    foreignKey: "id_client"
});
//moyen_communication -> client
dataBase.Moyen_communication.belongsTo(dataBase.Client, {
    foreignKey: "id_client",
    target:"id"
});
dataBase.Client.hasMany(dataBase.Moyen_communication, {
    foreignKey: "id_client"
});
//produit -> categorie
dataBase.Produit.belongsTo(dataBase.Categorie, {
    foreignKey: "id_categorie",
    target:"id"
});
dataBase.Categorie.hasMany(dataBase.Produit, {
    foreignKey: "id_categorie"
});
//produit -> marque
dataBase.Produit.belongsTo(dataBase.Marque, {
    foreignKey: "id_marque",
    target:"id"
});
dataBase.Marque.hasMany(dataBase.Produit, {
    foreignKey: "id_marque"
});
//produit -> reduction
dataBase.Produit.belongsTo(dataBase.Reduction, {
    foreignKey: "id_reduction",
    target:"id"
});
dataBase.Reduction.hasMany(dataBase.Produit, {
    foreignKey: "id_reduction"
});
//produit -> pays
dataBase.Produit.belongsTo(dataBase.Pays, {
    foreignKey: "id_pays",
    target:"id"
});
dataBase.Pays.hasMany(dataBase.Produit, {
    foreignKey: "id_pays"
});
//devis -> client
dataBase.Devis.belongsTo(dataBase.Client, {
    foreignKey: "id_client",
    target:"id"
});
dataBase.Client.hasMany(dataBase.Devis, {
    foreignKey: "id_client"
});
//ligne_devis -> produit
dataBase.Ligne_devis.belongsTo(dataBase.Produit, {
    foreignKey: "id_produit",
    target:"id"
});
dataBase.Produit.hasMany(dataBase.Ligne_devis, {
    foreignKey: "id_produit"
});
//ligne_devis -> tva
dataBase.Ligne_devis.belongsTo(dataBase.Tva, {
    foreignKey: "id_tva",
    target:"id"
});
dataBase.Tva.hasMany(dataBase.Ligne_devis, {
    foreignKey: "id_tva"
});
//ligne_devis -> devis
dataBase.Ligne_devis.belongsTo(dataBase.Devis, {
    foreignKey: "id_devis",
    target:"id"
});
dataBase.Devis.hasMany(dataBase.Ligne_devis, {
    foreignKey: "id_devis"
});
//facture -> devis
dataBase.Facture.belongsTo(dataBase.Devis, {
    foreignKey: "id_devis",
    target:"id"
});
dataBase.Devis.hasMany(dataBase.Facture, {
    foreignKey: "id_devis"
});
//facture -> client
dataBase.Facture.belongsTo(dataBase.Client, {
    foreignKey: "id_client",
    target:"id"
});
dataBase.Client.hasMany(dataBase.Facture, {
    foreignKey: "id_client"
});
//facture -> adresse
dataBase.Facture.belongsTo(dataBase.Adresse, {
    foreignKey: "id_adresse",
    target:"id"
});
dataBase.Adresse.hasMany(dataBase.Facture, {
    foreignKey: "id_adresse"
});
//ligne_facture -> produit
dataBase.Ligne_facture.belongsTo(dataBase.Produit, {
    foreignKey: "id_produit",
    target:"id"
});
dataBase.Produit.hasMany(dataBase.Ligne_facture, {
    foreignKey: "id_produit"
});
//ligne_facture -> tva
dataBase.Ligne_facture.belongsTo(dataBase.Tva, {
    foreignKey: "id_tva",
    target:"id"
});
dataBase.Tva.hasMany(dataBase.Ligne_facture, {
    foreignKey: "id_tva"
});
//ligne_facture -> facture
dataBase.Ligne_facture.belongsTo(dataBase.Facture, {
    foreignKey: "id_facture",
    target:"id"
});
dataBase.Facture.hasMany(dataBase.Ligne_facture, {
    foreignKey: "id_facture"
});
//commande -> moyen_paiement
dataBase.Commande.belongsTo(dataBase.Moyen_paiement_utilise, {
    foreignKey: "id_moyen_paiement",
    target:"id"
});
dataBase.Moyen_paiement_utilise.hasMany(dataBase.Commande, {
    foreignKey: "id_moyen_paiement"
});
//commande -> client
dataBase.Commande.belongsTo(dataBase.Client, {
    foreignKey: "id_client",
    target:"id"
});
dataBase.Client.hasMany(dataBase.Commande, {
    foreignKey: "id_client"
});
//commande -> facture
dataBase.Commande.belongsTo(dataBase.Facture, {
    foreignKey: "id_facture",
    target:"id"
});
dataBase.Facture.hasMany(dataBase.Commande, {
    foreignKey: "id_facture"
});
//livrer -> facture
dataBase.Livrer.belongsTo(dataBase.Facture, {
    foreignKey: "id_facture",
    target:"id"
});
dataBase.Facture.hasMany(dataBase.Livrer, {
    foreignKey: "id_facture"
});
//livrer -> adresse
dataBase.Livrer.belongsTo(dataBase.Adresse, {
    foreignKey: "id_adresse",
    target:"id"
});
dataBase.Adresse.hasMany(dataBase.Livrer, {
    foreignKey: "id_adresse"
});
//commentaire -> client
dataBase.Commentaire.belongsTo(dataBase.Client, {
    foreignKey: "id_client",
    target:"id"
});
dataBase.Client.hasMany(dataBase.Commentaire, {
    foreignKey: "id_client"
});
//commentaire -> produit
dataBase.Commentaire.belongsTo(dataBase.Produit, {
    foreignKey: "id_produit",
    target:"id"
});
dataBase.Produit.hasMany(dataBase.Commentaire, {
    foreignKey: "id_produit"
});
//posseder -> client
dataBase.Posseder.belongsTo(dataBase.Client, {
    foreignKey: "id_client",
    target:"id"
});
dataBase.Client.hasMany(dataBase.Posseder, {
    foreignKey: "id_client"
});
//posseder -> adresse
dataBase.Posseder.belongsTo(dataBase.Adresse, {
    foreignKey: "id_adresse",
    target:"id"
});
dataBase.Adresse.hasMany(dataBase.Posseder, {
    foreignKey: "id_adresse"
});
//posseder -> type_adresse
dataBase.Posseder.belongsTo(dataBase.Type_adresse, {
    foreignKey: "id_type_adresse",
    target:"id"
});
dataBase.Type_adresse.hasMany(dataBase.Posseder, {
    foreignKey: "id_type_adresse"
});
//




/*chargement configuration JSON des actions --> controleurs */
global.dynamicRouter = JSON.parse(fs.readFileSync("./routes/dynamicRoute.json", 'utf8'));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({
    extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


//gestion dynamique des routes
require('./dynamicRouter')(app);

// catch 404 and forward to error handler
app.use((req, res, next) => {
    next(createError(404));
});

// error handler
app.use((err, req, res, next) => {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
