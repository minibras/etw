const express = require('express');
const router = express.Router();
const dataBase = require('../models/models');

/* GET home page. */
router.get('/', (req, res, next) => {
    dataBase.Ville.findAll({
        where: {
            id_code_postal: 1
        },
        include: [dataBase.Code_postal]
    }).then(ville => {
        res.render(req.message.view, {
            title: 'test.js requete',
            result: JSON.stringify(ville),
            result2: ville
        });
    }).catch(error => {
        console.log("erreur lors du findAll : " + error);
    });
});

module.exports = router;

