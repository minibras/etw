var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render(req.message.view, { title: 'InfinityApp' });
});

module.exports = router;
