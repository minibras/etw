const express = require("express");
const router = express.Router();
const url = require("url");

function dynamicRouter(app) {
    // automatisme
    router.use(manageAction);
    app.use(router);
}

function manageAction(req, res, next) {
    let path; //le pathname (après le 3000 dans l'url)
    let type; // la méthode (get post etc... methode http)
    let controler; //nom du controleur à charger
    path = url.parse(req.url).pathname;
    type = req.method;
    //il faut supprimer pour le routage le paramètre apres l'action
    if (path.split('/').length > 0) path = '/' + path.split('/')[1]

    // configuration du message pour les contrôleurs génériques
    req.message = {};
    req.message.action = type + path;
    if (global.dynamicRouter[type + path].view){
        req.message.view = global.dynamicRouter[type + path].view;
    } else {
        req.message.view = null;
    }  

    if (typeof global.dynamicRouter[type + path] == 'undefined') {
        console.log("erreur pas d'action : " + path);
        next();
    } else {
        instanceModule = require('./routes/' + global.dynamicRouter[type + path].controler);
        router.use(path, instanceModule);
        next();
    }
}

module.exports = dynamicRouter;
